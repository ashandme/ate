#ifndef BLACK
#define BLACK "#073642"
#endif
#ifndef RED
#define RED "#dc322f"
#endif
#ifndef GREEN
#define GREEN "#869900"
#endif
#ifndef YELLOW
#define YELLOW "#b58900"
#endif
#ifndef BLUE
#define BLUE "#268bd2"
#endif
#ifndef MAGENTA
#define MAGENTA "#d33682"
#endif
#ifndef CYAN
#define CYAN "#2aa198"
#endif
#ifndef WHITE
#define WHITE "#93a1a1"
#endif

#ifndef BRIGHT_BLACK
#define BRIGHT_BLACK "#002b36"
#endif
#ifndef BRIGHT_RED
#define BRIGHT_RED "#cb4b16"
#endif
#ifndef BRIGHT_GREEN
#define BRIGHT_GREEN "#586e75"
#endif
#ifndef BRIGHT_YELLOW
#define BRIGHT_YELLOW "#657b83"
#endif
#ifndef BRIGHT_BLUE
#define BRIGHT_BLUE "#839496"
#endif
#ifndef BRIGHT_MAGENTA
#define BRIGHT_MAGENTA "#6c71c4"
#endif
#ifndef BRIGHT_CYAN
#define BRIGHT_CYAN "#93a1a1"
#endif
#ifndef BRIGHT_WHITE
#define BRIGHT_WHITE "#fdf6e3"
#endif

#ifndef BACKGROUND_COLOR
#define BACKGROUND_COLOR "#002b36"
/// for ligth theme:
// #define BACKGROUND_COLOR "#fdf6e3"
#endif
#ifndef FOREGROUND_COLOR
#define FOREGROUND_COLOR "#839496"
// #define FOREGROUND_COLOR "#657b83"
#endif

#ifndef BACKGROUND_OPACITY
#define BACKGROUND_OPACITY 0.9
#endif

#ifndef INITIAL_FONT_SIZE
#define INITIAL_FONT_SIZE 14
#endif

#ifndef SCROLL_SIZE
#define SCROLL_SIZE 300
#endif

#ifndef FONT_FAMILY
#define FONT_FAMILY "DejaVu Sans Mono"
#endif

#ifndef RESET_FONT_KEYVAL
#define RESET_FONT_KEYVAL "0"
#endif
#ifndef RESET_FONT_MODIFIER_MASK
#define RESET_FONT_MODIFIER_MASK GDK_CONTROL_MASK
#endif

#ifndef INCREMENT_FONT_KEYVAL
#define INCREMENT_FONT_KEYVAL "plus"
#endif
#ifndef INCREMENT_FONT_MODIFIER_MASK
#define INCREMENT_FONT_MODIFIER_MASK GDK_CONTROL_MASK | GDK_SHIFT_MASK
#endif

#ifndef DECREMENT_FONT_KEYVAL
#define DECREMENT_FONT_KEYVAL "underscore"
#endif
#ifndef DECREMENT_FONT_MODIFIER_MASK
#define DECREMENT_FONT_MODIFIER_MASK GDK_CONTROL_MASK | GDK_SHIFT_MASK
#endif

#ifndef PASTE_PRIMARY_KEYVAL
#define PASTE_PRIMARY_KEYVAL "y"
#endif
#ifndef PASTE_PRIMARY_MODIFIER_MASK
#define PASTE_PRIMARY_MODIFIER_MASK GDK_MOD1_MASK | GDK_SHIFT_MASK
#endif

#ifndef PASTE_CLIPBOARD_KEYVAL
#define PASTE_CLIPBOARD_KEYVAL "v"
#endif
#ifndef PASTE_CLIPBOARD_MODIFIER_MASK
#define PASTE_CLIPBOARD_MODIFIER_MASK GDK_CONTROL_MASK | GDK_SHIFT_MASK
#endif

#ifndef PIPECMD_KEYVAL
#define PIPECMD_KEYVAL "u"
#endif
#ifndef PIPECMD_MODIFIER_MASK
#define PIPECMD_MODIFIER_MASK GDK_MOD1_MASK | GDK_SHIFT_MASK
#endif

#ifndef MODAL_INPUT_KEYVAL
#define MODAL_INPUT_KEYVAL "colon"
#endif
#ifndef MODAL_INPUT_MODIFIER_MASK
#define MODAL_INPUT_MODIFIER_MASK GDK_CONTROL_MASK
#endif
